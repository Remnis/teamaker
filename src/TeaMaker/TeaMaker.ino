/*
 * TeaMaker
 * Copyright (c)2019-2022 Agis Wichert
 * http://www.rohling.de
 * https://www.youtube.com/nenioc187
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const char Version[] = "1.1";
const char Copyright[] = "(c)2019 ";
const char Author[] = "Agis Wichert";

// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ESPName
char* EspName = "IoTeaMaker";

// WiFi & MQTT Server
const char* ssid = "<SSID>";
const char* password = "<Password>";
const char* mqtt_server = "192.168.1.99";
const char* mqtt_user = "mqtt-user";
const char* mqtt_password = "mqtt";

// MQTT Subscribtions
const char* ModeSubscribe = "teamaker/mode/in"; // ESP recieves here mode info
const char* ModePublish = "teamaker/mode/out";

const char* TimerSubscribe = "teamaker/timer/in"; // ESP recieves here mode info
const char* TimerPublish = "teamaker/timer/out";

const char* RemainingPublish = "teamaker/remain/out";

WiFiClient espClient;
PubSubClient pubClient(espClient);

long lastPublish = 0;
int publishInterval = 2350;
long lastCheck = 0;

// modes
byte teaMode = 0; // 0 - Sleep, 1 - Standby, 2 - making tea
int teaTimer = 0; // in seconds
long teaStart = 0; // start time
byte teaPercent = 0;

// Servo
#include <Servo.h>
Servo teaServo;
byte servoDelay = 10;
byte servoPos = 255;
byte servoMax = 180;
byte servoMin = 90;
byte servoSleep = 10;
byte servoDirection = 1;
#define ServoPin D3


void setup() {
  Serial.begin(115200);
  Serial.print("IoTeamaker ");
  Serial.println(Version);
  Serial.println(Copyright);
  // WiFi
  setup_wifi();
  
  // MQTT Server
  Serial.print("Connecting to MQTT...");
  // connecting to the mqtt server
  pubClient.setServer(mqtt_server, 1883);
  pubClient.setCallback(callback);
  Serial.println(F("done!"));
  
  // reset last publish timer
  lastPublish = millis();
  lastCheck = millis();
  Serial.print(F("Attaching servo.."));
  teaServo.attach(ServoPin);
  Serial.println(F("done"));
}

void loop() {
  if (!pubClient.connected()) {
    delay(100);
    reconnect();
  }
  pubClient.loop();
  
  // checking mode
  if((lastCheck + servoDelay) < millis()){
    //Serial.println("checking mode");
    checkMode();
    lastCheck = millis();
  }

  // publish topics
  if((lastPublish + publishInterval) < millis()){
   // Serial.println("publih topics");
    publishTopics();
    lastPublish = millis();
  }
}

void checkMode(){
  if(teaMode == 0){
    // sleep Mode
    if(servoPos != servoSleep){
      servoPos--;
      teaServo.write(servoPos);
    }
  }
  else if(teaMode == 1){
    // Standby
    if(servoPos < servoMax){
      servoPos++;
      teaServo.write(servoPos);
    }
    //Serial.println("Standby");
  }
  else if(teaMode == 2){
    // dip tea
    if(servoPos > servoMin){
      servoPos--;
      teaServo.write(servoPos);
    }
    // check if tea is ready
    if((teaStart + (teaTimer * 1000)) < millis()){
      teaMode = 1;
    }
  }
}


void callback(char* topic, byte* payload, unsigned int length) {
  if(String(topic).equals(String(ModeSubscribe))){
    // changing mode
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    teaMode = m.toInt();
    if(teaMode == 2){
      // start making tea
      teaStart = millis();
    }
    Serial.println(m);
  }
  else if(String(topic).equals(String(TimerSubscribe))){
    // changing timer
    
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    teaTimer = m.toInt();
    if(teaMode == 2){
      // start making tea
      teaStart = millis();
    }
    Serial.print(m);
  }
  
}

// publish topics to sync with all devices
void publishTopics(){
    Serial.print("Publish mode...");
    // mode
    char b[2];
    String str;
    str=String(teaMode);
    str.toCharArray(b,2);
    pubClient.publish( ModePublish, b );  
    Serial.println("done");

    Serial.print("Publish timer...");
    //publish timer
    char t[4];
    str=String(teaTimer);
    str.toCharArray(t,4);
    pubClient.publish( TimerPublish, t );
    Serial.println("done");

    Serial.print("Publish remaining time");
    //publish timer remaining in percent
    int perc = 0;
    if(teaMode == 2){
      perc = (100 * (millis()-teaStart)) / (teaTimer * 1000);
      if((teaStart + (teaTimer * 1000)) <= millis()){
        perc = 100;
      }
    }
    else {
      perc = 0;
    }
    if(perc != teaPercent){
      str=String(perc);
      str.toCharArray(t,4);
      pubClient.publish( RemainingPublish, t );
      teaPercent = perc;
    }
    Serial.println("done");
    
}


// MQTT server connect
void reconnect() {
  // try to reconnect
  if (!pubClient.connected()) {
    // Attempt to connect
    if (pubClient.connect(EspName, mqtt_user, mqtt_password)) {
      Serial.println(F("MQTT connected"));

      // subsrcibe to mode
      pubClient.subscribe(ModeSubscribe);
      pubClient.publish(ModePublish, "0");
      pubClient.loop();

      // subsrcibe to timer
      pubClient.subscribe(TimerSubscribe);
      pubClient.publish(TimerPublish, "0");
      pubClient.loop();

      // publish remaining timer
      pubClient.publish(RemainingPublish, "0");
      pubClient.loop();

      Serial.println(F("Done"));
      
    } else {
      Serial.print(F("failed, rc="));
      Serial.print(pubClient.state());
      Serial.println(F(" try again in 5 seconds"));
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


// Network connection
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println(F("WiFi connected"));
  Serial.println(F("IP address: "));
  Serial.println(WiFi.localIP());
}
